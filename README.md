# SprykerCommerceOperatingSystem


This is the repository of the SprykerCommerceOperatingSystem Cloud Team.

## WHO WE ARE
short introduction of each team members

## WHAT WE DO
short introduction of the product we develop

## HOW WE WORK
short description on how we work <br>
[DEFINITION OF DONE](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/blob/main/team/definition-of-done.md) <br>
[OUR BOARD](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/boards)

## PRODUCT RELEASES
- [v1.0.0](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/tags/v1.0.0)
- [v1.1.0](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/tags/v1.1.0)
- [v2.0.0](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/tags/v2.0.0)
- [v2.0.1](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/tags/v2.0.1): **current**
- [v2.1.0](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/tags/v2.1.0): **next**
- [v2.1.1](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/tags/v2.1.1): **next**

## SPRINTS
- [Sprint 10](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/milestones/1#tab-issues)
- [Sprint 11](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/milestones/2#tab-issues): **current**
- [Sprint 12](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/milestones/3#tab-issues): **upcoming**
- [Sprint 13](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/milestones/4#tab-issues): **upcoming**
