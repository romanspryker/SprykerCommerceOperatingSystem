# FEATURE REQUIREMENTS DOCUMENT

| :black_circle: | :large_blue_circle: |
| ------ | ------ |
| author | PO Cloud Platform Team |
| reviewer | Technical Lead Cloud Platform Team |
| reviewer | Platform Architect Cloud Platform Team|


- 1-07-2022: `STATUS: SUBMITTED by PO Cloud Platform Team` :arrow_forward:
- 2-07-2022: `STATUS: APPROVED by Lead Cloud Platform Operations` :heavy_check_mark:

## ASSUMPTIONS

- Ops is able to detect failure but not to check on health of CD pipelines
- Ops have full access to logs of CD pipelines
- Ops Team has a sandbox environment to test the enhanced version of pipelines
  - **EDIT: Current sandbox is not up-to-date. Ops committed to patch and upgrade it in order to have an environment to test the new features**
- Ops Team has enough capacity to onboard the new pipelines (no holiday period, no covid outbreak into the team)
- CD Pipelines execution is synchronous, a pipelines execution either fails or succeed

## GOAL
Main goal is to build a new product release based upon the feature described by this document which will give to Operations the ability to monitor the CD pipelines in order to be able to check on healthiness of pipelines and prevent error from happening.

**MAIN BENEFITS FOR THE PRODUCT:**

| FROM | TO | VALUE |
| ------ | ------ |------ |
| error handling | error prevention | - minimize failure rate of CD pipelines |
| reactive failure management | proactive failure management | - lower weekly amount of tickets issued by customers <br> - improve customer satisfaction rate |

### USE CASES

| ACTOR |  FEATURE | ACCEPTANCE CRITERIA |
| ------ | ------ | ------ |
| OPS ENGINEER | As a Ops Engineer I want to have monitor capabilites for our CD pipelines so that I can monitor status of execution and resource availability (near) real-time | - The feature _must_ come with built-in default dashboards to proper visualize metrics and status of pipelines <br> - The new feature should be easy to learn. A user manual _must_ be provided in order to better understand the new monitor capabilities and being able to modify or add new dashboards when needed <br> - The feature _must_ come with a notification mechanism to notify the Ops team when something is not going as it should <br> - The feature _must not_ introduce any security vulnerabilities <br> - The feature _must not_ introduce breaking changes
| CUSTOMER DEVELOPER | As a developer I want to have the possibility to deploy my code whenever I want into the SprykerOS so that I can keep my productity high and not wasting time in troubleshooting CD pipelines | - The feature _should_ lower the current failure rate of CD pipelines of 90% in the next 3 months <br> - The new feature _must not_ have impact on the availability of CD pipeline and or slower its execution time.


## ACTION PLAN 
The PO, together with the Cloud Product Team, commit to:
- Define **monitoring areas** within the CD pipelines
  - Operations identified that memory consumption is the most common reason of CD pipelines failure
- For each area, define **metrics**
- For each metrics, define specific **thresholds**
- Build the following product characteristics:
  - CD pipelines will be **observable-by-design** based upon open-sources technologies
  - **Health monitoring alarms and notification** are in scope of this feature
  - **Dashboards** reporting status of pipelines and main metrics are in scope of this feature
  - **Manual of use** is in scope of this feature

## DEPENDENCIES AND CONSTRAINTS
- The availability of a Sandbox environment to test the enhanced pipelines is a requirement for the Operations Team. They commit to make the Sanbox environment ready as soon as possible. 
- Only open-source tools are eligible to develop this feature.
