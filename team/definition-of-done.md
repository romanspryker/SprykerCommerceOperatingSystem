# Definition Of Done

- Tests written and passing
- Continuous Integration build passing
- Security vulnerability scan passing
- Cross-browser testing done on current top 5 browsers according to analytics
- Code peer-reviewed
- Documentation updated
- Acceptance criteria met
