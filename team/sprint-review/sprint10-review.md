# SPRINT10 REVIEW

- **Date**: 2022-06-30
- **Facilitator**: James Bond, SM of the Cloud Platform Team

| PARTICIPANTS | ROLE |
| ------ | ------ |
| Cloud Platform Team | _The Team_ |
| Robert de Niro | Team Cloud Operations Lead |
| Minnie Mouse | Customer Success Manager |
| Mr. Mark | Director of Marketing |
| Juergen Albertsen | Director Product for Cloud| 


## DEMO

The Cloud Platform Team demoed the new features of the product.

## FEEDBACK

The feedback from stakeholders are all positive, the new features really add value to the platform but there are some complains:
- The Customer Success Manager reports that several customers are experiencing intermittent availability of CD pipelines. Some customers were even unable to deliver updated versions of their webshop and this is lowering the customer satisfaction rate (no penalties are in place as no SLA are defined).
- The Team Lead of Cloud Operations reports that the failure rate of CD pipelines is 60% but they have not been provided by the development team of any means to proper monitor the CD pipelines. With current situation they can't improve the infrastructure on top of which the pipelines are running and failure rate can't be improved.

## ACTION POINTS
- @theteam: Due to necessity of Operations and the instability of CD pipelines, The Cloud Platform Team Operations commits to a new feature to build monitoring capabilities for our CD pipelines, so that Operations can understand and fix the problems before the customers report them.
- @ProductOwner: Will produce ASAP a requirement document describing the new feature and validate it with the Operations Team Lead.
- @ProductOwner: Will prioritize and plan delivery of the new feature accordingly with the priority HIGH given by the Director.
- @ProductOwner: Will send to stakeholders the delivery plan so that everyone is aware of when the feature will be available. This will be done after having discussed the main requirements with the team.

